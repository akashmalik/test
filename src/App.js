import React, { Component } from 'react'
// import logo from './logo.svg';
import './App.css';
import './assets/css/slick.css'
import './assets/css/styles.css'
import './assets/css/slick-theme.css'
// import './assets/css/bootstrap.min.css'
import Routes from './routing/route'

class Custom extends Component {
  render() {
    return (
      <>
        
        <Routes />
        
      </>
    );
  }
}

export default Custom;
