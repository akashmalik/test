import { ApiRequest } from "../../apiServices/ApiRequest";

export const simpleAction = () => dispatch => {
    console.log('action')
    dispatch({
        type: 'TEST',
        payload: 'result_of_simple_action'
    })
}

export const GETQuesAns = () => dispatch => {
    ApiRequest('', '/getQuesAns', 'GET', '', '')
        .then((res) => {
            if (res.status === 201 || res.status === 200) {
                dispatch({
                    type: 'GETQuesAns',
                    payload: res.data
                })
            }
        })
        .catch((error) => {
            console.log(error)
        })
}