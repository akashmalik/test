import { combineReducers } from 'redux';
import {simpleAction} from '../actions/actions'

const defaultState = {
    testState : 'Initial Data',
    categories: []
}

function reducer(state = defaultState, action){
    console.log("reducer -==>>", action, state)
    switch (action.type) {
        case 'TEST':
            return {
                ...state,
                testState: action.payload
            }
            break
        case 'GETFCATEGORIES':
            return {
                ...state,
                categories: action.payload
            }
            break
        default: return state
    }
}

export default reducer