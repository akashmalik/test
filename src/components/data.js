import booms from '../assets/img/category/booms.png'
import scissor_lifts from '../assets/img/category/scissor_lifts.png'
import scissor_lift_slab from '../assets/img/category/subcategory/scissor_lift_slab.png'
import forklifts from '../assets/img/category/forklifts.png'
import single_man_lifts from '../assets/img/category/single_man_lifts.png'
import carry_deck_crane from '../assets/img/category/carry_deck_crane.png'
import personnel_cart from '../assets/img/category/personnel_cart.png'
import facility_and_maintenance from '../assets/img/category/material_lifts.png'
import earth_moving from '../assets/img/category/earth_moving.png'
import material_lifts from '../assets/img/category/material_lifts.png'

import booms_articulated_electric from '../assets/img/category/subcategory/booms_articulated_electric.png'
import booms_articulated_engine from '../assets/img/category/subcategory/booms_articulated_engine.png'
import forklift_electric from '../assets/img/category/subcategory/forklift_electric.png'
import forklift_ic_cushion from '../assets/img/category/subcategory/forklifts_ic_cushion.png'
import forklift_ic_pneumatic from '../assets/img/category/subcategory/forklift_ic_pneumatic.png'
import single_man_lifts_driveable from '../assets/img/category/subcategory/single_man_lifts_driveable.png'
import booms_straight from '../assets/img/category/subcategory/booms_straight.png'
import forklift_rough_terrain from '../assets/img/category/subcategory/forklift_rough_terrain.png'
import scissor_lift_rough_terrain from '../assets/img/category/subcategory/scissor_lift_rough_terrain.png'
import single_man_lifts_push_around from '../assets/img/category/subcategory/single_man_lifts_push_around.png'
import telehandlers from '../assets/img/category/telehandlers.png'
import personnel_cart_rough_terrain from '../assets/img/category/subcategory/personnel_cart_rough_terrain.png'
import forklift_tugger from '../assets/img/category/subcategory/forklift_tugger.png'
import forklift_swingmast from '../assets/img/category/subcategory/forklift_swingmast.png'
import earth_moving_skid_steer from '../assets/img/category/subcategory/earth_moving_skid_steer.png'
import booms_towable from '../assets/img/category/subcategory/booms_towable.png'
import earth_moving_backhoe from '../assets/img/category/subcategory/earth_moving_backhoe.png'
import earth_moving_compact_truck_loader from '../assets/img/category/subcategory/earth_moving_compact_truck_loader.png'
import earth_moving_mini_excavator from '../assets/img/category/subcategory/earth_moving_mini_excavator.png'
import forklift_piggyback from '../assets/img/category/subcategory/forklift_piggyback.png'

export const data = {
    "status": "success",
    "data": {
        "locations": [
            {
                "dealers_id": "EDIL",
                "opco": "EDIL",
                "name": "ILLINOIS",
                "branches": [
                    {
                        "branch_id": "BUR",
                        "name": "Burr Ridge",
                        "categories": [
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "ELG",
                        "name": "Itasca",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "ROC",
                        "name": "Rockford",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": forklift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDIN",
                "opco": "EDKY",
                "name": "INDIANA",
                "branches": [
                    {
                        "branch_id": "EVA",
                        "name": "Evansville",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDKY",
                "opco": "EDKY",
                "name": "KENTUCKY",
                "branches": [
                    {
                        "branch_id": "BOW",
                        "name": "Bowling Green",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": forklift_rough_terrain
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "HOP",
                        "name": "Hopkinsville",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "LOU",
                        "name": "Louisville",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Facility Maintenance & Cleaning",
                                "image": facility_and_maintenance,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "PAD",
                        "name": "Paducah",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "LEX",
                        "name": "Lexington",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDNJ",
                "opco": "EDPA",
                "name": "NEW JERSEY",
                "branches": [
                    {
                        "branch_id": "MTL",
                        "name": "Mt. Laurel",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDOH",
                "name": "OHIO",
                "opco": "OHIO",
                "branches": [
                    {
                        "branch_id": "MON",
                        "name": "Monroe",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "CIN",
                        "name": "Cincinnati",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "DAY",
                        "name": "Dayton",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "LEB",
                        "name": "Lebanon",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDPA",
                "opco": "EDPA",
                "name": "PENNSYLVANIA",
                "branches": [
                    {
                        "branch_id": "ALN",
                        "name": "Allentown",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Earth Moving",
                                "image": earth_moving,
                                "subcategories": [
                                    {
                                        "name": "Skidsteer",
                                        "image": earth_moving_skid_steer
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "LAN",
                        "name": "Lancaster",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "MEC",
                        "name": "Mechanicsburg",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "WIL",
                        "name": "Williamsport",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDTX",
                "opco": "EDTX",
                "name": "TEXAS",
                "branches": [
                    {
                        "branch_id": "AUS",
                        "name": "Austin",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Atrium Lift"
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    },
                                    {
                                        "name": "Towable",
                                        "image": booms_towable
                                    }
                                ]
                            },
                            {
                                "name": "Earth Moving",
                                "image": earth_moving,
                                "subcategories": [
                                    {
                                        "name": "Backhoe - 2WD",
                                        "image": earth_moving_backhoe
                                    },
                                    {
                                        "name": "Compact Track Loader",
                                        "image": earth_moving_compact_truck_loader
                                    },
                                    {
                                        "name": "Mini-Excavator",
                                        "image": earth_moving_mini_excavator
                                    },
                                    {
                                        "name": "Skidsteer",
                                        "image": earth_moving_skid_steer
                                    }
                                ]
                            },
                            {
                                "name": "Facility Maintenance & Cleaning",
                                "image": facility_and_maintenance,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Piggy Back",
                                        "image": forklift_piggyback
                                    }
                                ]
                            },
                            {
                                "name": "Material Lift",
                                "image": material_lifts,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    },
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "BEA",
                        "name": "Beaumont",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "BRA",
                        "name": "Brazosport",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "CCH",
                        "name": "Corpus Christi",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Earth Moving",
                                "image": earth_moving,
                                "subcategories": [
                                    {
                                        "name": "Mini-Excavator",
                                        "image": earth_moving_mini_excavator
                                    },
                                    {
                                        "name": "Skidsteer",
                                        "image": earth_moving_skid_steer
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "DAL",
                        "name": "Dallas",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "FOR",
                        "name": "Ft. Worth",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    },
                                    {
                                        "name": "Atrium Lift"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Piggy Back",
                                        "image": forklift_piggyback
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": forklift_rough_terrain
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "HOU",
                        "name": "Houston",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "LON",
                        "name": "Longview",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": forklift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "MCA",
                        "name": "McAllen",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    },
                                    {
                                        "name": "Towable",
                                        "image": booms_towable
                                    }
                                ]
                            },
                            {
                                "name": "Earth Moving",
                                "image": earth_moving,
                                "subcategories": [
                                    {
                                        "name": "Compact Track Loader",
                                        "image": earth_moving_compact_truck_loader
                                    },
                                    {
                                        "name": "Skidsteer",
                                        "image": earth_moving_skid_steer
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "SAN",
                        "name": "San Antonio",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Earth Moving",
                                "image": earth_moving,
                                "subcategories": [
                                    {
                                        "name": "Backhoe - 2WD",
                                        "image": earth_moving_backhoe
                                    },
                                    {
                                        "name": "Mini-Excavator",
                                        "image": earth_moving_mini_excavator
                                    },
                                    {
                                        "name": "Skidsteer",
                                        "image": earth_moving_skid_steer
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Piggy Back",
                                        "image": forklift_piggyback
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Material Lift",
                                "image": material_lifts,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "SHE",
                        "name": "Sherman",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Piggy Back",
                                        "image": forklift_piggyback
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "branch_id": "WAC",
                        "name": "Waco",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    },
                                    {
                                        "name": "Atrium Lift"
                                    },
                                    {
                                        "name": "Towable",
                                        "image": booms_towable
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": forklift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Material Lift",
                                "image": material_lifts,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "dealers_id": "EDWV",
                "opco": "OHIO",
                "name": "WEST VIRGINIA",
                "branches": [
                    {
                        "branch_id": "NIT",
                        "name": "Nitro",
                        "categories": [
                            {
                                "name": "Booms",
                                "image": booms,
                                "subcategories": [
                                    {
                                        "name": "Articulated - Electric",
                                        "image": booms_articulated_electric
                                    },
                                    {
                                        "name": "Articulated - Engine",
                                        "image": booms_articulated_engine
                                    },
                                    {
                                        "name": "Straight",
                                        "image": booms_straight
                                    }
                                ]
                            },
                            {
                                "name": "Carry Deck Crane",
                                "image": carry_deck_crane,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Forklifts",
                                "image": forklifts,
                                "subcategories": [
                                    {
                                        "name": "Electric",
                                        "image": forklift_electric
                                    },
                                    {
                                        "name": "IC Cushion",
                                        "image": forklift_ic_cushion
                                    },
                                    {
                                        "name": "IC Pneumatic",
                                        "image": forklift_ic_pneumatic
                                    },
                                    {
                                        "name": "Swing Mast",
                                        "image": forklift_swingmast
                                    },
                                    {
                                        "name": "Tugger",
                                        "image": forklift_tugger
                                    }
                                ]
                            },
                            {
                                "name": "Scissor Lifts",
                                "image": scissor_lifts,
                                "subcategories": [
                                    {
                                        "name": "Slab",
                                        "image": scissor_lift_slab
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": scissor_lift_rough_terrain
                                    }
                                ]
                            },
                            {
                                "name": "Single Man Lifts",
                                "image": single_man_lifts,
                                "subcategories": [
                                    {
                                        "name": "Driveable",
                                        "image": single_man_lifts_driveable
                                    },
                                    {
                                        "name": "Push Around",
                                        "image": single_man_lifts_push_around
                                    }
                                ]
                            },
                            {
                                "name": "Telehandlers",
                                "image": telehandlers,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    }
                                ]
                            },
                            {
                                "name": "Personnel Cart",
                                "image": personnel_cart,
                                "subcategories": [
                                    {
                                        "name": "NA"
                                    },
                                    {
                                        "name": "Rough Terrain",
                                        "image": personnel_cart_rough_terrain
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "error": {}
}