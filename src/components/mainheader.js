import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import { data } from "./data";
const main_data = data.data.locations
class header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dealer:'',
            final_branches:[]
        }
        this.choose_branch = this.choose_branch.bind(this);
    }
    choose_branch(dealer){
        var branch = []
        let product_data = main_data.filter((item)=>item.dealers_id === dealer)[0]
        product_data.branches.map((item,index)=>{
            branch.push(item)
        })
        console.log('branch',branch)
        this.setState({
            dealer:dealer,
            final_branches : branch
        })
    }
 

    componentDidMount() {

    }
    render() {
        return (
            <div className="header">
                <h4 style={{color:'white'}}>Rental Managnment System</h4>
                <Dropdown style={{float:'right'}}>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Location
  </Dropdown.Toggle>
                    <Dropdown.Menu>
                        {
                            main_data.map((item, index) => {
                                return (
                                <div>
                                    <Dropdown.Item onClick={this.choose_branch.bind(this,item.dealers_id)} >{item.name}</Dropdown.Item>
                                </div>
                                )
                            })
                        }
                    </Dropdown.Menu>
                </Dropdown>
                {
                    this.state.final_branches &&  this.state.final_branches.length>0 ? 
                    <Dropdown style={{float:'right'}}>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Choose Branch
  </Dropdown.Toggle>
  {/* href={"#/product/" + item.dealers_id} */}
                    <Dropdown.Menu>
                        {
                            this.state.final_branches.map((item) => {
                                return (<div>
                                    
                                    <Dropdown.Item href={"#/product/" + this.state.dealer + '/' + item.branch_id} onClick={()=>{this.props.changebranch()}}>{item.name}</Dropdown.Item>

                                </div>
                                )
                            })
                        }
                    </Dropdown.Menu>
                </Dropdown>
                :''
                }
            </div>
        )
    }
}



export default (header)