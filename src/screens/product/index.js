import React from 'react';
import Header from "../../components/mainheader";
import imgg from '../../assets/img/category/booms.png';
import { data } from '../../components/data';
const main_data = data.data.locations
class Home extends React.Component {
    constructor(props) {
        super(props);
        var path = this.props.location.pathname;
        var branch = path.split('/')[3]
        var location = path.split('/')[2]
        this.state = {
            branch : branch,
            location : location,
            final_product : [],
            sub_categories:[]
        }
        this.view_subcategory = this.view_subcategory.bind(this);
    }
    view_subcategory=(data)=>{
        this.setState({
            sub_categories: data.subcategories
        })
    }
    componentDidMount() {
        let branch_data = main_data.filter((item)=>item.dealers_id === this.state.location)[0]
        let product_data = branch_data.branches.filter((item1)=>item1.branch_id === this.state.branch)[0]
        console.log('product_dataproduct_data',product_data.categories)
        this.setState({
            final_product : product_data.categories
        })
    }


    render() {
        return (
            <div>
                <Header changebranch={()=>{setTimeout(() => {
                                        
                    window.location.reload()
                }, 500);}}></Header>
               
                {
                    this.state.sub_categories.length > 0   ? 
                    this.state.sub_categories.map((item1,index)=>
                    <div>
                        
                        <img src={item1.image} />
                        {item1.name}
                        </div>
                    )
                    :
                    <div>
                {
                    this.state.final_product   ? 
                    this.state.final_product.map((item,index)=>
                    <div>
                        
                        <img src={item.image}/>
                        <button onClick={()=>this.view_subcategory(item)}>{item.name}</button>
                        </div>
                    )
                    :''
                }
                </div>
                }
                
            </div>
        )
    }
}


export default (Home)