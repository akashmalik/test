export const siteUrl = 'http://localhost:3000/api/';


// api call common function
export const ApiRequest = (variables, endUrl, apiMethod, token, id) => {
    var init ={}
    if (id === 'apiWithImage') {
        init = {
            method: apiMethod === 'PUT' ? 'PUT' : apiMethod === 'PATCH' ? 'PATCH' : '',
            headers: {
                'Authorization': token ? `Bearer ${token}` : ''
            },
            body: variables
        }
    } else {
        init = apiMethod === 'GET' ? {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                // 'token': token ? token : ''
            }
        } :
            {
                method: apiMethod === 'POST' ? 'POST' : apiMethod === 'PATCH' ? 'PATCH' : '',
                headers: {
                    'Content-Type': 'application/json',
                    // 'token': token ? token : ''
                },
                body: JSON.stringify(variables)
            }
    }
    console.log('hitting on -=>', siteUrl + endUrl, init)
    return fetch(siteUrl + endUrl, init)
        .then(res => res.json()
            .then(data => {
                var apiData = {
                    status: res.status,
                    data: data
                }
                console.log('Api Response -=>', apiData)
                return apiData
            }))
        .catch(err => {
            console.log('err -=>' + JSON.stringify(err))
            var apiData = {
                status: 900,
                data: 'Please check your internet connection'
            }
            return apiData
        })
}