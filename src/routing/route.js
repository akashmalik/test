import React, { Component, Suspense } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

const Home = React.lazy(() => import('../screens/home/index'))
const Product = React.lazy(() => import('../screens/product/index'))

// app routes
class Routes extends Component {
  render() {
    return (
      <HashRouter >
        <Suspense fallback={''}>
          <Switch >
            <Route path="/" component={Home} exact />
            <Route path="/product" component={Product} />
            
          </Switch>
        </Suspense>
      </HashRouter>
    );
  }
}

export default Routes;